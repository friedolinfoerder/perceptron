
if(!Array.isArray) {
    Array.isArray = function (value) {
        return toString.call(value) === '[object Array]';
    }
}


Matrix = (function() {
    
    var constr,
        eachRow = function(callback) {
            var data = this._data,
                m = this.m;
                
            for(var r = 0; r < m; r++) {
                callback.call(data[r], r, data);
            }
            
            // fluent interface
            return this;
        },
        each = function(callback) {
            var data = this._data,
                m = this.m,
                n = this.n;
                
            for(var r = 0; r < m; r++) {
                for(var c = 0; c < n; c++) {
                    callback.call(data[r][c], r, c, data);
                }
            }
            
            // fluent interface
            return this;
        },
        collectRow = function(callback) {
            var data = this._data,
                m = this.m,
                n = this.n;
                
            for(var r = 0; r < m; r++) {
                data[r] = callback.call(data[r], r, data);
            }
        },
        collect = function(callback) {
            var data = this._data,
                m = this.m,
                n = this.n;
                
            for(var r = 0; r < m; r++) {
                for(var c = 0; c < n; c++) {
                    data[r][c] = callback.call(data[r][c], r, c, data);
                }
            }
        },
        columns = function(a, b) {
            var data = this._data,
                m = this.m,
                newData = [],
                sliceArgs = [a];
            
            if(b !== undefined) {
                sliceArgs.push(b);
            }
            
            for(var r = 0; r < m; r++) {
                var row = data[r];
                
                newData.push(row.slice.apply(row, sliceArgs));
            }
            
            return this.data(newData);
        },
        copy = function() {
            return new Matrix(this._data);
        },
        empty = function() {
            return (this.m == 0 || this.n == 0);
        },
        rows = function(a, b) {
            var data = this._data,
                newData = [],
                sliceArgs = [a-1];
            
            if(b !== undefined) {
                sliceArgs.push(b-1);
            }
            data.slice.apply(data, sliceArgs);
            
            return this.data(newData);
        },
        _action = function(matrix, callback) {
            var m = this.m,
                n = this.n;
            
            if(typeof matrix === "number") {
                this.collect(function() {
                    return callback.call(this, matrix);
                });
            } else if(this.shape() == matrix.shape()) {
                this.collect(function(i, j) {
                    return callback.call(this, matrix._data[i][j]);
                });
            } else {
                throw new Error("The shapes are not aligned: "+this.shape()+" and "+matrix.shape());
            }
            
            // fluent interface
            return this;
        },
        add = function(matrix) {
            return _action.call(this, matrix, function(val) {
                return this + val;
            });
        },
        sub = function(matrix) {
            return _action.call(this, matrix, function(val) {
                return this - val;
            });
        },
        mult = function(matrix) {
            return _action.call(this, matrix, function(val) {
                return this * val;
            });
        },
        transpose = function() {
            var data = this._data,
                newData = [],
                m = this.m,
                n = this.n;
            for(var c = 0; c < n; c++) {
                var newRow = [];
                for(var r = 0; r < m; r++) {
                    newRow.push(data[r][c]);
                }
                newData.push(newRow);
            }
            return this.data(newData);
        },
        flatten = function() {
            var data = this._data,
                newData = [],
                m = this.m,
                n = this.n;
                
            for(var r = 0; r < m; r++) {
                for(var c = 0; c < n; c++) {
                    newData.push(data[r][c]);
                }
            }
            return newData;
        },
        /**
         * @example data(newData)
         * @example data(3, 2)
         */
        data = function(a, b) {
            if(a === undefined) {
                return this._data;
            }
            if(typeof a === "number") {
                return this._data[a][b];
            } else if(Array.isArray(a)) {
                this.m = a.length;
                this.n = 0;
                if(a.length > 0) {
                    if(!Array.isArray(a[0])) {
                        this.m = 1;
                        a = [a];
                    }
                    this.n = a[0].length;
                }
                this._data = a;
            }
            
            // fluent interface
            return this;
        },
        shape = function() {
            return "("+this.m.toString()+", "+this.n.toString()+")";
        },
        push = function(data) {
            this._data.push(data);
            
            // fluent interface
            return this;
        },
        pop = function() {
            return this._data.pop();
        },
        prepend = function(data) {
            var func;
            if(typeof data === "number") {
                this.eachRow(function() {
                    this.unshift(data);
                });
            } else {
                this.collectRow(function(i) {
                    return data[i].concat(this);
                });
            }
            
            // fluent interface
            return this;
        },
        append = function(data) {
            var func;
            if(typeof data === "number") {
                this.eachRow(function() {
                    this.push(data);
                });
            } else {
                this.collectRow(function(i) {
                    return this.concat(data[i]);
                });
            }
            
            
            // fluent interface
            return this;
        };
    
    constr = function(rows, columns, initial) {
        if(Array.isArray(rows)) {
            this.data(rows);
            return;
        }
        this.m = rows;
        this.n = columns;
        
        var data = this._data = [],
            val = (initial === undefined) ? 1 : initial;
            
        for(var r = rows; r--; ) {
            var row = [];
            for(var c = columns; c--; ) {
                row.push(val);
            }
            data.push(row);
        }
    };
    
    // PUBLIC API
    constr.prototype = {
        constructor: "Matrix",
        add: add,
        sub: sub,
        mult: mult,
        transpose: transpose,
        flatten: flatten,
        columns: columns,
        data: data,
        copy: copy,
        empty: empty,
        shape: shape,
        each: each,
        eachRow: eachRow,
        collect: collect,
        collectRow: collectRow,
        push: push,
        pop: pop,
        append: append,
        prepend: prepend
    };
    
    return constr;
}());