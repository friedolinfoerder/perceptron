
perceptron = (function() {
    
    var constr,
        // dependencies
        Neuron,
        Connection,
        Layer,
        activation,
        decision,
        Linear,
        Sigmoid,
        setterGetter,
        // helper
        setValues = function(inputs) {
            var layers = this.layers,
                layersLength = layers.length,
                activation = null,
                layer = layers[0],
                neurons = layer.neurons(),
                neuron = null,
                connections = null,
                connection = null;
                
            // Set values of neurons
            layer.values(inputs);
            
            
            // iterate over all other layers
            for(var l = 1; l < layersLength; l++) {
                layer = layers[l];
                activation = layer.activation();
                neurons = layer.neurons();
                
                var k = 0;
                if(layer._bias) {
                    k = 1;
                    neurons[0].val(1);
                }

                // iterate over all neurons
                for(var length = neurons.length; k < length; k++) {
                    neuron = neurons[k];

                    connections = neuron.ins();

                    // iterate over all input connections
                    var sum = 0;
                    for(var j = 0, insLength = connections.length; j < insLength; j++) {
                        connection = connections[j];
                        sum += connection.val()*connection.input().val();
                    }
                    neuron.val(activation.val(sum));
                }
            }
        },
        buildConnections = function(i, j) {
            var leftLayer = this.layers[i],
                rightLayer = this.layers[j],
                leftNeurons = leftLayer.neurons(),
                rightNeurons = rightLayer.neurons();
                
            
            for(var li = 0, leftLength = leftNeurons.length; li < leftLength; li++) {
                leftNeurons[li].outs([]);
            }
            for(var ri = 0, rightLength = rightNeurons.length; ri < rightNeurons; ri++) {
                rightNeurons[ri].ins([]);
            }
            
            // bias got no inputs
            var startRi = rightLayer.bias() ? 1 : 0;
            
            for(li = 0, leftLength = leftNeurons.length; li < leftLength; li++) {
                var left = leftNeurons[li];
                for(ri = startRi, rightLength = rightNeurons.length; ri < rightLength; ri++) {
                    var right = rightNeurons[ri],
                        connection = new Connection();
                    
                    connection
                        .input(left)
                        .output(right);
                    
                    
                    left._outs.push(connection);
                    right._ins.push(connection);
                }
            }
        };
    
    /**
     * @example perceptron()
     * @example perceptron(data)
     * @example perceptron(input, output)
     */
    constr = function() {
        var args = Array.prototype.slice.call(arguments, 0),
            last = args[args.length-1],
            userOptions = {},
            options = {
                output: "Linear",
                decision: "Linear"
            };
            
        if(typeof last === "object" && !Array.isArray(last)) {
            userOptions = args.pop();
        }
        for(var o in userOptions) {
            options[o] = userOptions[o];
        }
        
        
        this._input = new Layer()
            .activation(new Linear());
        this._output = new Layer()
            .activation(new activation[options.output]())
            .bias(false);
        this.layers = [this._input, this._output];
        this._decision = new decision[options.decision]();
        
        this._initialized = false;
        
        if(args.length > 0) {
            var cnt = typeof args[0] === "object" ? args[0].length : args[0];
            this.input(cnt);
        }
        if(args.length > 1) {
            var cnt = typeof args[args.length-1] === "object" ? args[args.length-1].length : args[args.length-1];
            this.output(cnt);
        }
        if(args.length > 2) {
            this.hidden.apply(this, args.slice(1, -1));
        }
        
        this._initialized = true;
        for(var j = 0, layerLength = this.layers.length; j < layerLength-1; j++) {
            buildConnections.call(this, j, j+1);
        }
    };
    
    // PUBLIC API
    constr.prototype = {
        constructor: "perceptron",
        input: function(cnt) {
            if(cnt === undefined) {
                return this._input;
            }
            if(this._input._bias) {
                cnt++;
            }
            
            var neurons = [];
            
            // add neuron
            for(var i = cnt; i--; ) {
                neurons.push(new Neuron());
            }
            
            // set neurons
            this._input.neurons(neurons);
            
            if(this._initialized) {
                buildConnections.call(this, 0, 1);
            }
            
            // fluent interface
            return this;
        },
        output: function(cnt) {
            if(cnt === undefined) {
                return this._output;
            }
            if(this._output._bias) {
                cnt++;
            }
            
            var neurons = [];
            
            // add neuron
            for(var i = cnt; i--; ) {
                neurons.push(new Neuron());
            }
            
            // set neurons
            this._output.neurons(neurons);
            
            if(this._initialized) {
                buildConnections.call(this, this.layers.length-2, this.layers.length-1);
            }
            
            // fluent interface
            return this;
        },
        /**
         * @example hidden(2)
         * @example hidden(5, 3, 7)
         */
        hidden: function() {
            var args = Array.prototype.slice.call(arguments, 0);
            
            // remove hidden layers
            this.layers.splice(1, this.layers.length - 2);
            
            // add hidden layers
            for(var i = 0, length = args.length; i < length; i++) {
                var cnt = typeof args[i] === "object" ? args[i].length : args[i],
                    layer = new Layer(new Sigmoid()),
                    neurons = [];
                
                if(layer._bias) {
                    cnt++;
                }
                
                for(var j = cnt; j--; ) {
                    neurons.push(new Neuron());
                }
                
                layer.neurons(neurons);
                
                this.layers.splice(this.layers.length-1, 0, layer);
            }
            
            if(this._initialized) {
                for(var l = 0, layerLength = this.layers.length; l < layerLength-1; l++) {
                    buildConnections.call(this, l, l+1);
                }
            }
            
            // fluent interface
            return this;
        },
        decision: function(value) {
            return setterGetter.call(this, "decision", value);
        },
        predict: function(data) {
            this.setValues(data);
            return this._decision.val(this._output.values());
        },
        weights: function(data) {
            var weights = [],
                layers = this.layers;
                
            for(var l = 0, layersLength = layers.length; l < layersLength-1; l++) {
                var layer = layers[l],
                    neurons = layer.neurons();
                    
                for(var k = 0, length = neurons.length; k < length; k++) {
                    var outs = neurons[k].outs();
                    
                    for(var o = 0, outsLength = outs.length; o < outsLength; o++) {
                        weights.push(outs[o].val());
                    }
                }
            }
            return weights;
        },
        setValues: setValues
    };
    
    constr._setDependencies = function() {
        Neuron = perceptron.Neuron;
        Connection = perceptron.Connection;
        activation = perceptron.activation;
        decision = perceptron.decision;
        Linear = activation.Linear;
        Sigmoid = activation.Sigmoid;
        Layer = perceptron.Layer;
        setterGetter = perceptron.utils.setterGetter;
    };
    
    return constr;
}());

perceptron.utils = {
    setterGetter: function(name, value) {
        if(value === undefined) {
            return this["_"+name];
        }
        this["_"+name] = value;

        // fluent interface
        return this;
    }
};

perceptron.Neuron = (function() {
    
    var constr,
        setterGetter = perceptron.utils.setterGetter;

    constr = function() {
        this._val = 1;
        this._delta = 0;
        this._ins = [];
        this._outs = [];
    };

    constr.prototype = {
        constructor: "perceptron.neuron",
        ins: function(connections) {
            return setterGetter.call(this, "ins", connections);
        },
        outs: function(connections) {
            return setterGetter.call(this, "outs", connections);
        },
        val: function(value) {
            return setterGetter.call(this, "val", value);
        },
        delta: function(value) {
            return setterGetter.call(this, "delta", value);
        }
    };

    return constr;
}());

perceptron.Connection = (function() {
    
    var constr,
        setterGetter = perceptron.utils.setterGetter;
    
    constr = function(max) {
        max = max || 0.01;
        this._val = Math.random()*max*2-max;
    };
    
    constr.prototype = {
        constructor: "perceptron.Connection",
        input: function(neuron) {
            return setterGetter.call(this, "input", neuron);
        },
        output: function(neuron) {
            return setterGetter.call(this, "output", neuron);
        },
        val: function(value) {
            return setterGetter.call(this, "val", value);
        }
    };
    
    return constr;
}());

perceptron.activation = {
    Linear: (function() {
        var constr;
        
        constr = function(k) {
            this.k = (k === undefined) ? 1 : k;
        };
        
        constr.prototype = {
            constructor: "perceptron.activation.Linear",
            val: function(x) {
                return this.k*x;
            },
            derivation: function(x) {
                return this.k;
            }
        };
        
        return constr;
    }()),
    Sigmoid: (function() {
        var constr;
        
        constr = function(k) {
            this.k = (k === undefined) ? 1 : k;
        };
        
        constr.prototype = {
            constructor: "perceptron.activation.Sigmoid",
            val: function(x) {
                return 1/(1+Math.exp(-this.k*x));
            },
            derivation: function(x) {
                return x*(1-x);
            }
        }
        
        return constr;
    }()),
    Sign: (function() {
        var constr;
        
        constr = function() {
        };
        
        constr.prototype = {
            constructor: "perceptron.activation.Sign",
            val: function(x) {
                return (x >= 0) ? 1 : 0;
            },
            derivation: function(x) {
                return 1;
            }
        }
        
        return constr;
    }())
};

perceptron.decision = {
    Threshold: (function() {
        var constr;
        
        constr = function(k) {
            this.k = (k === undefined) ? 0.5 : k;
        };
        
        constr.prototype = {
            constructor: "perceptron.decision.Threshold",
            val: function(values) {
                return (values[0] >= this.k) ? 1 : 0;
            }
        };
        
        return constr;
    }()),
    Linear: (function() {
        var constr;
        
        constr = function() {
            
        };
        
        constr.prototype = {
            constructor: "perceptron.decision.Linear",
            val: function(values) {
                return values[0];
            }
        };
        
        return constr;
    }()),
    Maximum: (function() {
        var constr;
        
        constr = function() {
            
        };
        
        constr.prototype = {
            constructor: "perceptron.decision.Linear",
            val: function(values) {
                return Math.max.apply(this, values).indexOf(values);
            }
        };
        
        return constr;
    }())
};

perceptron.Layer = (function() {
    
    var constr,
        setterGetter = perceptron.utils.setterGetter;
    
    constr = function(act) {
        this._neurons = [];
        this._bias = true;
        
        if(act !== undefined) {
            this._activation = act;
        } else {
            this._activation = null;
        }
        
    };
    
    constr.prototype = {
        constructor: "perceptron.Layer",
        bias: function(value) {
            return setterGetter.call(this, "bias", value);
        },
        neurons: function(neurons) {
            return setterGetter.call(this, "neurons", neurons);
        },
        activation: function(activation) {
            return setterGetter.call(this, "activation", activation);
        },
        data: function(data) {
            return setterGetter.call(this, "data", data);
        },
        values: function(values) {
            var neurons = this.neurons(),
                i = 0,
                length = neurons.length;
                
            if(values === undefined) {
                var output = [];
                if(this._bias) {
                    i = 1;
                }
                for(; i < length; i++) {
                    output.push(neurons[i].val());
                }
                return output;
            }
            if(this._bias) {
                values.unshift(1);
            }
            for(; i < length; i++) {
                neurons[i].val(values[i]);
            }
            
            // fluent interface
            return this;
        }
    };
    
    return constr;
}());

perceptron.Trainer = (function() {
    
    var constr,
        setterGetter = perceptron.utils.setterGetter;
    
    constr = function(net, X, y, options) {
        this.X = X;
        this.y = y;
        this.net = net;
        this._epochs = 100;
        this.alpha = 0.1;
        
        // merge options
        if(typeof options === "object") {
            for(var o in options) {
                this[o] = options[o];
            }
        }
    };
    
    constr.prototype = {
        constructor: "perceptron.Trainer",
        epochs: function(value) {
            return setterGetter.call(this, "epochs", value);
        },
        train: function(callback) {
            var net = this.net,
                X = this.X,
                y = this.y;
            
            if(!(X instanceof Matrix)) {
                X = new Matrix(X);
            }
            if(!(y instanceof Matrix)) {
                y = new Matrix(y);
            }
            if(X.m != y.m) {
                throw new Error("inputs and output count are not the same: "+X.m+" != "+y.m);
            }

            var _this = this,
                layers = net.layers,
                layersLength = layers.length,
                layer = null,
                activation = null,
                connections = null,
                connection = null,
                neurons = null,
                neuron,
                alpha = this.alpha;
                
            for(var count = this._epochs; count--; ) {
                X.eachRow(function(i) {
                    layer = net._input;
                    neurons = layer.neurons();
                    
                    // 1. Set values of neurons
                    net.setValues(this);

                    // 2. Set delta of neurons
                    layer = net._output;
                    neurons = layer.neurons();
                    for(var k = 0, length = neurons.length; k < length; k++) {
                        neuron = neurons[k];
                        activation = layer.activation();

                        neuron.delta(y.data(i, k) - neuron.val());
                    }
                    
                    for(var l = layersLength - 1; l > 0; l--) {
                        layer = layers[l];
                        activation = layer.activation();
                        neurons = layer.neurons();

                        for(k = 0, length = neurons.length; k < length; k++) {
                            neuron = neurons[k];

                            connections = neuron.outs();

                            // iterate over all output connections
                            var outSum = 0;
                            for(var o = 0, outsLength = connections.length; o < outsLength; o++) {
                                connection = connections[o];
                                outSum += connection.val()*connection.output().delta();
                            }
                            neuron.delta(outSum*activation.derivation(neuron.val()));
                        }
                    }
                    
                    // 3. Set weights
                    for(l = 0; l < layersLength - 1; l++) {
                        layer = layers[l];
                        neurons = layer.neurons();


                        for(k = 0, length = neurons.length; k < length; k++) {
                            neuron = neurons[k];
                            
                            connections = neuron.outs();
                            
                            for(var p = 0, pLength = connections.length; p < pLength; p++) {
                                connection = connections[p];
                                connection.val(connection.val() + alpha*connection.input().val()*connection.output().delta());
                            }
                        }
                    }
                });
            }
            if(callback instanceof Function) {
                callback.call(this);
            }
            
            // fluent interface
            return this;
        }
    };
    
    return constr;
}());
    

// set dependencies
perceptron._setDependencies();